package com.gojolo.telefonsakasi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


public class MainActivity extends Activity {
	private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(MainActivity.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/7845745841");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
		GridView gridView = (GridView) findViewById(R.id.grid);
        // Instance of ImageAdapter Class
        gridView.setAdapter(new ImageAdapter(this));
        /**
         * On Click event for Single Gridview Item
         * */
        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                    int position, long id) {
            	   Intent i = getIntent();
               if(position==0)
                   {
                   	Intent intent = new Intent(MainActivity.this,R1.class);
                   	startActivity(intent);
                   }
                if(position==1)
                   {
                   	Intent intent = new Intent(MainActivity.this,R2.class);
                   	startActivity(intent);
                   }
                  if(position==2)
                   {
                   	Intent intent = new Intent(MainActivity.this,R3.class);
                   	startActivity(intent);
                   }
                   if(position==3)
                   {
                   	Intent intent = new Intent(MainActivity.this,R4.class);
                   	startActivity(intent);
                   }
                   if(position==4)
                   {
                   	Intent intent = new Intent(MainActivity.this,R5.class);
                   	startActivity(intent);
                   }
                   if(position==5)
                   {
                   	Intent intent = new Intent(MainActivity.this,R6.class);
                   	startActivity(intent);
                   }
                   if(position==6)
                   {
                   	Intent intent = new Intent(MainActivity.this,R7.class);
                   	startActivity(intent);
                   }
                   if(position==7)
                   {
                   	Intent intent = new Intent(MainActivity.this,R8.class);
                   	startActivity(intent);
                   }
                // passing array index
                i.putExtra("id", position);
                startActivity(i);
            }
        });
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
